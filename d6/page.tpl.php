<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="<?php print $language->language; ?>" xml:lang="<?php print $language->language; ?>">

<head>
  <title><?php print $head_title; ?></title>
  <?php print $head; ?>
  <?php print $styles; ?>
  <!--[if IE 7]>
    <link rel="stylesheet" href="<?php print $base_path . $directory; ?>/ie7-fixes.css" type="text/css">
  <![endif]-->
  <!--[if lte IE 6]>
    <link rel="stylesheet" href="<?php print $base_path . $directory; ?>/ie6-fixes.css" type="text/css">
  <![endif]-->
  <?php print $scripts; ?>
</head>

<body class="<?php print $body_classes; ?>">

<div id="topbar">
	
	<div id="adminpanel">
		<ul>
			<li>Hello <strong><?php global $user; { echo "".$user->name."";}?></strong></li>
			<li><a href="<?php echo base_path(); ?>user">My Account</a></li>
			<li><a href="<?php echo base_path(); ?>admin/help">Help</a></li>
			<li class="last"><a href="<?php echo base_path(); ?>logout">Sign Out</a></li>
		</ul>
	</div><!-- /admin panel -->
	
	<div id="siteinfo">
    <strong><?php print $site_name ?></strong><span class="viewsite">( <a href="<?php echo base_path(); ?>">view site &raquo;</a> )</span>
  </div><!-- /site info -->
	
</div><!-- /top bar -->

<div id="page">
		
	<div id="branding">

    <?php if ($search_box): ?>
		<div id="searchbar">
			<?php print $search_box ?>
		</div>
    <?php endif; ?>

    <?php if ($logo): ?>
		<div id="logo">
      <a href="<?php print $base_path ?>" title="<?php print t('Home') ?>"><img src="<?php print $logo ?>" alt="<?php print t('Home') ?>" /></a>
    </div><!-- /logo -->
    <?php endif; ?>

	</div><!-- /branding -->

	<div id="admin-navigation">
		<ul class="links primary-links">
			<li><a href="<?php echo base_path(); ?>admin">Admin</a></li>
			<li><a href="<?php echo base_path(); ?>node/add">Create Content</a></li>
			<li><a href="<?php echo base_path(); ?>admin/build/block">Blocks</a></li>
			<li><a href="<?php echo base_path(); ?>admin/build/menu">Menus</a></li>
			<li><a href="<?php echo base_path(); ?>admin/user/user">Users</a></li>		
		</ul>
	</div><!-- /admin-menu -->

  <div id="main" class="clearfix">  
    <div id="main-inner" class="clearfix">

      <div id="content-wrapper" class="clearfix">
        <div id="content-inner" class="clearfix">

          <?php if ($breadcrumb): ?>
          <div id="breadcrumbwrapper">
            <div class="youarehere"><strong><?php print t('You are here :'); ?></strong></div>
            <div id="breadcrumbs">
              <?php print $breadcrumb ?><?php if ($title != ""): ?><div class="breadcrumb"> &raquo; <?php print $title ?></div><?php endif; ?>
            </div>
          </div><!-- /breadcrumb -->
          <?php endif; ?>

          <?php if ($content_top):?>
          <div id="content-top">
            <div id="content-top-inner">
              <?php print $content_top; ?>
            </div>
          </div><!-- /content-top -->
          <?php endif; ?>

          <?php if ($tabs): ?>
          <div id="content-tabs" class="clear">
            <?php print $tabs; ?>
          </div>
          <?php endif; ?>
          <?php if ($title): ?>
          <h1 class="title"><?php print $title; ?></h1>
          <?php endif; ?>
          <?php if ($help): ?>
          <?php print $help; ?>
          <?php endif; ?>
          <?php if ($messages): ?>
          <?php print $messages; ?>
          <?php endif; ?>
          <?php if ($content): ?>
          <div id="content-content">
            <?php print $content; ?>
          </div><!-- /content-content -->
          <?php endif; ?>
                
          <?php if ($content_bottom): ?>
          <div id="content-bottom">
            <?php print $content_bottom; ?>
          </div><!-- /content-bottom -->
          <?php endif; ?>
                  
          <?php print $feed_icons; ?>
                 
        </div><!-- /content -->
      </div><!-- /content-wrapper -->

    </div><!-- /main-inner -->
  </div><!-- /main -->

  <?php if ($sidebar): ?>
  <div id="sidebar" class="sidebar clearfix">
    <div class="sidebar-inner">
      <?php print $sidebar; ?>
    </div>
  </div><!-- /sidebar -->
  <?php endif; ?>

	<div id="footer">
		<div id="rightfooter"><a href="<?php echo base_path(); ?>rss.xml" id="rssfeed" title="View RSS Feed" ><em>View RSS Feed</em></a></div>
		<?php print $footer_message ?>
	</div><!-- /footer -->
	
</div><!-- /page -->

<?php print $closure ?>		
</body>
</html>