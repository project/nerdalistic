<div class="divider">
	<?php if ($block->subject): ?>
	<h2><?php print $block->subject; ?></h2>
	<?php endif; ?>
	<div class="dividertext">
		<?php print $block->content; ?>
	</div>
</div>