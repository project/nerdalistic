  <div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
    <?php if ($picture) {
      print $picture;
    }?>
    <?php if ($page == 0) { ?><h2 class="title"><a href="<?php print $node_url?>"><?php print $title?></a></h2><?php }; ?>
    
    <div class="content">
    
   <?php if ($submitted) { ?>
    <div class="submitted">
        <?php
            print 'Last updated by ' . theme('username', $node) . ' on: ' . format_date($node->changed, 'custom', 'm/d/Y');
            if ($terms) {
                print ' | in ';
            }
        ?>
    </div>
    <?php } ?>    

    <?php print $content?></div>
    <?php if ($links) { ?><div class="links">&raquo; <?php print $links?></div><?php }; ?>
  </div>