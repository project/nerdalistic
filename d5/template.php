<?php

/**
 * @file
 * File which contains theme overrides for the nerdalistic theme.
 */

/*
 * ABOUT
 *
 *  The template.php file is one of the most useful files when creating or modifying Drupal themes.
 *  You can add new regions for block content, modify or override Drupal's theme functions, 
 *  intercept or make additional variables available to your theme, and create custom PHP logic.
 *  For more information, please visit the Theme Developer's Guide on Drupal.org:
 *  http://drupal.org/node/509
 */

 
/*
 * MODIFYING OR CREATING REGIONS
 *
 * Regions are areas in your theme where you can place blocks.
 * The default regions used in themes  are "left sidebar", "right sidebar", "header", and "footer",  although you can create
 * as many regions as you want.  Once declared, they are made available to the page.tpl.php file as a variable.  
 * For instance, use <?php print $header ?> for the placement of the "header" region in page.tpl.php.
 * 
 * By going to  the administer > site building > blocks page you can choose which regions various blocks should be placed.
 * New regions you define here will automatically show up in the drop-down list by their human readable name.
 */
 
 
/*
 * Declare the available regions implemented by this engine.
 *
 * @return
 *    An array of regions.  The first array element will be used as the default region for themes.
 *    Each array element takes the format: variable_name => t('human readable name')
 */
function nerdalistic_regions() {
  return array(
       'left' => t('left sidebar'),
       'content_top' => t('content top'),
       'content_bottom' => t('content bottom'),
  );
} 

/*
 * OVERRIDING THEME FUNCTIONS
 *
 *  The Drupal theme system uses special theme functions to generate HTML output automatically.
 *  Often we wish to customize this HTML output.  To do this, we have to override the theme function.
 *  You have to first find the theme function that generates the output, and then "catch" it and modify it here.
 *  The easiest way to do it is to copy the original function in its entirety and paste it here, changing
 *  the prefix from theme_ to nerdalistic_.  For example:
 *
 *   original:  theme_breadcrumb() 
 *   theme override:   nerdalistic_breadcrumb()
 *
 *  See the following example. In this theme, we want to change all of the breadcrumb separator links from  >> to ::
 *
 */

 /**
  * Return a themed breadcrumb trail.
  *
  * @param $breadcrumb
  *   An array containing the breadcrumb links.
  * @return a string containing the breadcrumb output.
  */
function nerdalistic_breadcrumb($breadcrumb) {
	if (!empty($breadcrumb)) {
		return '<div class="breadcrumb">'. implode(' :: ', $breadcrumb) .'</div>';
	}
}
 
 
/* 
 * CREATE OR MODIFY VARIABLES FOR YOUR THEME
 *
 *  The most powerful function available to themers is the _phptemplate_variables() function. It allows you
 *  to pass newly created variables to different template (tpl.php) files in your theme. Or even unset ones you don't want
 *  to use.
 *
 *  It works by switching on the hook, or name of the theme function, such as:
 *    - page
 *    - node
 *    - comment
 *    - block
 *
 * By switching on this hook you can send different variables to page.tpl.php file, node.tpl.php
 * (and any other derivative node template file, like node-forum.tpl.php), comment.tpl.php, and block.tpl.php
 *
 */

 
/**
 * Intercept template variables
 *
 * @param $hook
 *   The name of the theme function being executed
 * @param $vars
 *   A sequential array of variables passed to the theme function.
 */

function _phptemplate_variables($hook, $vars) {

  $function = 'phptemplate_preprocess_'. str_replace('-', '_', $hook);
  if (function_exists($function)) {
    $function($vars);
  }

  return $vars;
}

function phptemplate_preprocess_page(&$vars) {
	// Manipulate the $vars array here.
	// get the currently logged in user
	global $user;

	// An anonymous user has a user id of zero.      
	if ($user->uid > 0) {
	// The user is logged in.
		$vars['logged_in'] = TRUE;
	}
	else {
	// The user has logged out.
		$vars['logged_in'] = FALSE;
	}

	// Classes for body element. Allows advanced theming based on context
	// (home page, node of certain type, etc.)
	$body_classes = array();
	$body_classes[] = ($vars['is_front']) ? 'front' : 'not-front';
	$body_classes[] = ($vars['logged_in']) ? 'logged-in' : 'not-logged-in';
	if ($vars['node']->type) {
		// If on an individual node page, put the node type in the body classes
		$body_classes[] = 'node-type-'. $vars['node']->type;
	}
	if ($vars['sidebar_left'] && $vars['sidebar_right']) {
		$body_classes[] = 'both-sidebars';
	}
	elseif ($vars['sidebar_left']) {
		$body_classes[] = 'one-sidebar sidebar-left';
	}
	elseif ($vars['sidebar_right']) {
		$body_classes[] = 'one-sidebar sidebar-right';
	}
	else {
		$body_classes[] = 'no-sidebars';
	}
	if (!$vars['is_front']) {
	// Add unique classes for each page and website section
	// First, remove base path and any query string.
	global $base_path;
	list(,$path) = explode($base_path, $_SERVER['REQUEST_URI'], 2);
	// If clean URLs are off, strip remainder of query string.
	list($path,) = explode('&', $path, 2);
	// Strip query string.
	list($path,) = explode('?', $path, 2);
			$path = rtrim($path, '/');
	// Construct the id name from the path, replacing slashes with dashes.
	$full_path = str_replace('/', '-', $path);
	// Construct the class name from the first part of the path only.
	list($section,) = explode('/', $path, 2);
	$body_classes[] = nerdalistic_id_safe('page-'. $full_path);
	$body_classes[] = nerdalistic_id_safe('section-'. $section);
	}
	$vars['body_classes'] = implode(' ', $body_classes); // implode with spaces

	$theme_path = path_to_theme();
	      
	/**
	* CUSTOM CSS
	* To add a new theme stylesheet to the site, duplicate the following line
	* and update the name of the stylesheet accordingly.  You do not need
	* to add style.css, as it will be added automatically.
	* 
	* Once CSS caching is enabled, these will all be merged into a single file
	* for a faster page download.  
	*/
	//drupal_add_css($theme_path . '/style-mystylesheet.css', 'theme');
	drupal_add_css($theme_path . '/reset.css', 'theme');
	drupal_add_css($theme_path . '/screen.css', 'theme');
      
	// Leave this line alone.  It's what rebuilds the CSS based on the lines above
	$vars['css'] = drupal_add_css();
	      
	/**
	* REMOVE DEFAULT CSS
	* To remove a default stylesheet from the system, use a line like one of the
	* following.  $vars['css'] is the "base" portion of the array.  The next part
	* is the media type of the stylesheet (usually "all").  The next part is either
	* "module" or "theme", depending on whether it was added as part of the module
	* or theme.  The last part is the path to the CSS file.  
	* 
	* To see what values are available to be unset, uncomment the "dpr()" line below.
	* When you view the page, you will see a big ugly list of all the files in the array.
	* Comment it again when you're done to hide the ugliness.
	*/
	unset($vars['css']['all']['module']['modules/system/defaults.css']);
	      
	//dpr($vars['css']);
	// Leave this line alone.  It's what rebuilds the final CSS directive from the code above
	$vars['styles'] = drupal_get_css($vars['css']);
  
  //drupal_add_js($theme_path . '/scripts.js');
  
  $vars['scripts'] = drupal_get_js();
}

function phptemplate_preprocess_node(&$vars) {
	// Manipulate the $vars array here.
	// get the currently logged in user
	global $user;

	// set a new $is_admin variable
	// this is determined by looking at the currently logged in user and seeing if they are in the role 'admin'
	$vars['is_admin'] = in_array('admin', $user->roles);
	      
	$node_classes = array('node');
	if ($vars['sticky']) {
		$node_classes[] = 'sticky';
	}
	if (!$vars['node']->status) {
		$node_classes[] = 'node-unpublished';
	}
	$node_classes[] = 'ntype-'. nerdalistic_id_safe($vars['node']->type);
	// implode with spaces
	$vars['node_classes'] = implode(' ', $node_classes);
	}

	function phptemplate_preprocess_comment(&$vars) {
	$node = node_load($vars['comment']->nid);
	$vars['author_comment'] = $vars['comment']->uid == $node->uid ? TRUE : FALSE;  
}

/**
* Converts a string to a suitable html ID attribute.
* - Preceeds initial numeric with 'n' character.
* - Replaces space and underscore with dash.
* - Converts entire string to lowercase.
* - Works for classes too!
* 
* @param string $string
*  the string
* @return
*  the converted string
*/
function nerdalistic_id_safe($string) {
	if (is_numeric($string{0})) {
	// if the first character is numeric, add 'n' in front
		$string = 'n'. $string;
	}
	return strtolower(preg_replace('/[^a-zA-Z0-9-]+/', '-', $string));
}

/**
* Implementation of theme_menu_item().
*
* Add active class and custom id to current menu item links.
*/

function phptemplate_menu_item($mid, $children = '', $leaf = TRUE) {
  $item = menu_get_item($mid); // get current menu item
  //$path = drupal_get_normal_path($item['path']);
  //$getpath = $_GET['q'];
  // decide whether to add the active class to this menu item
  if ((drupal_get_normal_path($item['path']) == $_GET['q']) // if menu item path...
  || (drupal_is_front_page() && $item['path'] == '<front>')) { // or front page...
    $active_class = ' active'; // set active class
  } else { // otherwise...
    $active_class = ''; // do nothing
  }
  
  $attribs = isset($item['description']) ? array('title' => $item['description']) : array();
  $replace = array(' ', '&');
  $attribs['id'] = 'menu-'. str_replace($replace, '-', strtolower($item['title']));

  return '<li class="'. ($leaf ? 'leaf' : ($children ? 'expanded' : 'collapsed')) . $active_class .'" id="'. $attribs['id'] . '">'. menu_item_link($mid) . $children ."</li>\n";
}