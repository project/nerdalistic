<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">
<head>
  <?php print $styles ?>
	<!--[if lt IE 7]><link rel="stylesheet" type="text/css" href="<?php echo $base_path . $directory; ?>/ie6.css" /><![endif]-->  
	<!--[if IE 7]><link rel="stylesheet" type="text/css" href="<?php echo $base_path . $directory; ?>/ie7.css" /><![endif]-->
  <?php print $scripts ?>
	<?php print $head ?>
	<title><?php print $head_title ?></title>
</head>
<body>

<!-- START TOP BAR -->	
<div id="topbar">
	
	<!-- START ADMIN PANEL -->
	<div id="adminpanel">
		<ul>
			<li>Hello <b><?php global $user; { echo "".$user->name."";}?></b></li>
			<li><a href="<?php echo base_path(); ?>user">My Account</a></li>
			<li><a href="<?php echo base_path(); ?>admin/help">Help</a></li>
			<li class="last"><a href="<?php echo base_path(); ?>logout">Sign Out</a></li>
		</ul>
	</div>
	<!-- END ADMIN PANEL -->
	
	<!-- START SITE INFO -->
	<div id="siteinfo">
    <strong><?php print $site_name ?></strong><span class="viewsite">( <a href="<?php echo base_path(); ?>">view site &raquo;</a> )</span>
  </div>
	<!-- START SITE INFO -->
	
</div>
<!-- END TOP BAR -->	

<!-- START WRAPPER -->
<div id="wrapper">
		
	<!-- START BRANDING -->
	<div id="branding">
		<div id="searchbar">
			<?php print $search_box ?>
		</div>
		<div id="logo">
			<?php
	        // Prepare header
	        $site_fields = array();

	        if ($logo || $site_title) {
	            print '<h1><a href="'. check_url($base_path) .'" title="'. $site_title .'">';
	            if ($logo) {
	              print '<img src="'. check_url($logo) .'" alt="'. $site_title .'" id="logo" />';
	            }
	            print $site_html .'</a></h1>';
	          }
	        ?>
		</div>
	</div>
	<!-- END BRANDING -->

	<!-- START ADMIN MENU -->
	<div id="adminmenu">
		<ul class="links primary-links">
			<li><a href="<?php echo base_path(); ?>admin">Admin</a></li>
			<li><a href="<?php echo base_path(); ?>node/add">Create Content</a></li>
			<li><a href="<?php echo base_path(); ?>admin/build/block">Blocks</a></li>
			<li><a href="<?php echo base_path(); ?>admin/build/menu">Menus</a></li>
			<li><a href="<?php echo base_path(); ?>admin/user/user">Users</a></li>		
		</ul>
	</div>
	<!-- END ADMIN MENU -->

	<!-- START CONTENT -->		
	<div id="content">

		<!-- START SIDEBAR -->
		<div id="sidebar">
			<?php print $sidebar_left ?>
        </div>
		<!-- END SIDEBAR -->

		<!-- START MAIN -->		
		<div id="main">
			<?php if ($breadcrumb): ?>
			<div id="breadcrumbwrapper">
				<div class="youarehere"><strong>You are here:</strong></div>
				<div id="breadcrumbs">
					<?php print $breadcrumb ?><?php if ($title != ""): ?><div class="breadcrumb"> &raquo; <?php print $title ?></div><?php endif; ?>
				</div>
			</div>
			<?php endif; ?>
			<h1><?php print $title ?></h1>
			<div class="grayline"></div>
			<div class="tabs"><?php print $tabs ?></div>
			<?php print $help ?>
			<?php print $messages ?>
			<?php print $content; ?>
		</div>
		<!-- END MAIN -->		

	</div>
	<!-- END CONTENT -->		

	<!-- START FOOTER -->		
	<div id="footer">
		<div id="rightfooter"><a href="<?php echo base_path(); ?>rss.xml" id="rssfeed" title="View RSS Feed" ><em>View RSS Feed</em></a></div>
		<?php print $footer_message ?>
		</div>
	<!-- END FOOTER -->			
	
</div>
<!-- END WRAPPER -->

<?php print $closure ?>		
</body>
</html>